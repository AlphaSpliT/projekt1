#ifndef NAGYEGESZ_CPP
#define NAGYEGESZ_CPP

#include "NagyEgesz.h"

NagyEgesz::NagyEgesz() {
    this->size = 1;
    this->sign = 1;
    this->array = new int[1];
    this->array[0] = 0;
}

NagyEgesz::NagyEgesz(const char * _array) {
    if (_array[0] == '-') {
        this->sign = -1;
        this->size = strlen(_array) - 1;
        this->array = new int[this->size];
        for (int i = strlen(_array) - 1; i >= 1; --i) {
            this->array[this->size - i] = (_array[i] - '0');  
        }
    } else {
        this->sign = 1;
        this->size = strlen(_array);
        this->array = new int[this->size];
        for (int i = strlen(_array) - 1; i >= 0; --i) {
            this->array[this->size - i - 1] = (_array[i] - '0');
        }
    }
}

NagyEgesz::NagyEgesz(int _sign, int _size, const int * _array) {
    this->size = _size;
    this->sign = _sign;
    this->array = new int[this->size];
    for (int i = 0; i < this->size; ++i) {
        this->array[size - i - 1] = _array[i];
    }
}

NagyEgesz::NagyEgesz(const NagyEgesz& rhs) {
    this->size = rhs.size;
    this->sign = rhs.sign;
    this->array = new int[this->size];
    memcpy(this->array, rhs.array, sizeof(int) * this->size);
}

void NagyEgesz::print(ostream & os) {
    os << (this->sign == 1 ? "" : "-");
    for (int i = 0; i < this->size; ++i) {
        os << this->array[size - i - 1];
    }
    os << " ";
}

NagyEgesz NagyEgesz::operator+(NagyEgesz rhs) {
    if (this->sign == rhs.sign) {
	    NagyEgesz res = rhs;
 
	    for (int i = 0, carry = 0; i < (int) max(this->size, rhs.get_size()) || carry; ++i) {
            if (i == (int) res.get_size())
                res.push_back(0);
            res.get_array()[i] += carry + (i < (int) this->size ? this->array[i] : 0);
            carry = res.get_array()[i] >= 10;
            if (carry)
                res.get_array()[i] -= 10;
	    }
	    return res;
	}
    return *this - (-rhs);
}

NagyEgesz NagyEgesz::operator-(NagyEgesz rhs) {
    if (this->sign == rhs.sign) {
        if (this->abs() >= rhs.abs()) {
            NagyEgesz res = *this;
            for (int i = 0, carry = 0; i < rhs.size || carry; ++i) {
                res.array[i] -= carry + (i < rhs.size ? rhs.array[i] : 0);
                carry = res.array[i] < 0;
                if (carry) {
                    res.array[i] += 10;
                }
            }
            res.trim();
            return res;
        }
        return -(rhs - *this);
    }
    return *this + (-rhs);
}

NagyEgesz NagyEgesz::operator*(NagyEgesz rhs) {
    NagyEgesz lhs = (*this);
    NagyEgesz ret;

    if (lhs.get_sign() == rhs.get_sign()) {
        ret.get_sign() = 1;
    } else {
        ret.get_sign() = -1;
    }

    for (int i = 0; i < lhs.get_size(); ++i) {
        for (int j = 0; j < rhs.get_size(); ++j) {
            if (i + j < ret.get_size()) {
                ret.get_array()[i + j] += lhs.get_array()[i] * rhs.get_array()[j];
            } else {
                ret.push_back(lhs.get_array()[i] * rhs.get_array()[j]);
            }
        }
    }

    for (int i = 0; i < ret.get_size(); ++i) {
        if (i + 1 < ret.get_size()) {
            ret.get_array()[i + 1] += int(ret.get_array()[i] / 10);
        } else if (ret.get_array()[i] >= 10) {
            ret.push_back(int(ret.get_array()[i] / 10));
        } else {
            break;
        }
        ret.get_array()[i] %= 10;
    }
    ret.trim();
    return ret;
}

NagyEgesz NagyEgesz::operator/(NagyEgesz rhs) {
    NagyEgesz lhs = (*this);
    if (lhs == rhs) {
        return NagyEgesz("1");
    }

    if (rhs == NagyEgesz("0")){
        throw NullavalValoOsztas();
    }

    NagyEgesz to_ret;
    int end_sign = lhs.sign == rhs.sign ? 1 : -1;

    NagyEgesz divisor(rhs);
    lhs.sign = 1;
    divisor.sign = 1;
    rhs.sign = 1;

    if (lhs < (divisor)) {
        return NagyEgesz("0");
    }

    NagyEgesz m_cnt("0");

    while (lhs > NagyEgesz("0")) {
        if (lhs >= (divisor)) {
            lhs -= (divisor);
            to_ret += power_of_ten(m_cnt);
            (divisor) *= NagyEgesz("10");
            m_cnt += NagyEgesz("1");
        } else {
            divisor = rhs;
            m_cnt = NagyEgesz("0");
            if (lhs < (divisor)) {
                to_ret.trim();
                to_ret.get_sign() = end_sign;
                return to_ret;
            }
        }
        divisor.sign = 1;
    }

    to_ret.sign = end_sign;
    to_ret.trim();
    return to_ret;
}

bool NagyEgesz::operator<(NagyEgesz rhs) {
    
    if (this->sign != rhs.sign) {
        return this->sign < rhs.sign;
    }

    if (this->size != rhs.size) {
        return this->size * this->sign < rhs.size * rhs.sign;
    }

    for (int i = this->size - 1; i >= 0; --i) {
        if (this->array[i] != rhs.array[i]) {
            return this->array[i] * this->sign < rhs.array[i] * this->sign;
        }
    }

    return false;
}

NagyEgesz & NagyEgesz::operator+=(NagyEgesz rhs) {
    (*this) = (*this) + rhs;
    return (*this);
}

NagyEgesz & NagyEgesz::operator-=(NagyEgesz rhs) {
    (*this) = (*this) - rhs;
    return (*this);
}

NagyEgesz & NagyEgesz::operator*=(NagyEgesz rhs) {
    (*this) = (*this) * rhs;
    return (*this);
}

NagyEgesz & NagyEgesz::operator/=(NagyEgesz rhs) {
    (*this) = (*this) / rhs;
    return (*this);
}

bool NagyEgesz::operator>(NagyEgesz rhs) {
    return rhs < (*this);
}

bool NagyEgesz::operator<=(NagyEgesz rhs) {
    return !(rhs < (*this));
}

bool NagyEgesz::operator>=(NagyEgesz rhs) {
    return !(*this < rhs);
}

bool NagyEgesz::operator==(NagyEgesz rhs) {
    return !(*this < rhs) && !(rhs < *this);
}

bool NagyEgesz::operator!=(NagyEgesz rhs) {
    return *this < rhs || rhs < *this;
}

int & NagyEgesz::get_size() {
    return this->size;
}

int * NagyEgesz::get_array() {
    return this->array;
}

int & NagyEgesz::get_sign() {
    return this->sign;
}

void NagyEgesz::push_back(int value) {
    int * arr = new int[this->size + 1];
    memcpy(arr, this->array, sizeof(int) * this->size);
    arr[this->size] = value;
    this->array = new int[this->size + 1];
    memcpy(this->array, arr, sizeof(int) * (this->size + 1));
    ++this->size;
    delete[] arr;
}

void NagyEgesz::pop_back() {
    int * arr = new int[this->size - 1];
    memcpy(arr, this->array, sizeof(int) * (this->size - 1));
    --this->size;
    memcpy(this->array, arr, sizeof(int) * (this->size));
    delete[] arr;
}

NagyEgesz & NagyEgesz::operator=(NagyEgesz rhs) {
    this->size = rhs.size;
    this->sign = rhs.sign;
    
    delete[] this->array;
    
    this->array = new int[this->size];
    memcpy(this->array, rhs.array, sizeof(int) * this->size);

}

NagyEgesz NagyEgesz::abs() {
    NagyEgesz res = (*this);
    res.get_sign() *= res.get_sign();
    return res;
}

void NagyEgesz::trim() {
    int index = this->size - 1;
    while (index != 0 && !this->array[index]) {
        this->pop_back();
        --index;
    }
}

ostream & operator<<(ostream & os, NagyEgesz rhs) {
    rhs.print(os);
    return os;
}

NagyEgesz power_of_ten(NagyEgesz num) {
    string to_ret = "1";
    NagyEgesz i("0");
    while (i < num) {
        to_ret += "0";
        i = i + NagyEgesz("1");
    }
    return(NagyEgesz(static_cast<const char *>(to_ret.c_str())));
}

NagyEgesz NagyEgesz::osszead(const NagyEgesz& rhs) {
    return (*this) + rhs;
}

NagyEgesz NagyEgesz::kivon(const NagyEgesz& rhs) {
    return (*this) - rhs;
} 

NagyEgesz NagyEgesz::szoroz(const NagyEgesz& rhs) {
    return (*this) * rhs;
} 

NagyEgesz NagyEgesz::oszt(const NagyEgesz& rhs) {
    return (*this) / rhs;
}

void NagyEgesz::kiir() const {
    cout << (this->sign == 1 ? "" : "-");
    for (int i = 0; i < this->size; ++i) {
        cout << this->array[size - i - 1];
    }
    cout << endl;
}

#endif