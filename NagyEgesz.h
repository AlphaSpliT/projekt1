#ifndef NAGYEGESZ_H
#define NAGYEGESZ_H

#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;

class NagyEgesz;


NagyEgesz power_of_ten(NagyEgesz);

class NagyEgesz {
    int * array;
    int sign;
    int size;

public:
    class NullavalValoOsztas{};
    NagyEgesz();
    NagyEgesz(int, int, const int *);
    NagyEgesz(const char *);
    NagyEgesz(const NagyEgesz&);
    
    NagyEgesz operator+(NagyEgesz);
    NagyEgesz operator-(NagyEgesz);
    NagyEgesz operator*(NagyEgesz);
    NagyEgesz operator/(NagyEgesz);

    NagyEgesz & operator+=(NagyEgesz);
    NagyEgesz & operator-=(NagyEgesz);
    NagyEgesz & operator*=(NagyEgesz);
    NagyEgesz & operator/=(NagyEgesz);

    NagyEgesz & operator=(NagyEgesz);

    NagyEgesz operator-() {
        NagyEgesz res = *this;
        res.get_sign() = -res.get_sign();
        return res;    
    }

    NagyEgesz osszead(const NagyEgesz&);
    NagyEgesz kivon(const NagyEgesz&);
    NagyEgesz szoroz(const NagyEgesz&);
    NagyEgesz oszt(const NagyEgesz&);

    NagyEgesz abs();

    int * get_array();
    int & get_size();
    int & get_sign();

    bool operator<(NagyEgesz);
    bool operator>(NagyEgesz);
    bool operator<=(NagyEgesz);
    bool operator>=(NagyEgesz);
    bool operator==(NagyEgesz);
    bool operator!=(NagyEgesz);

    void print(ostream&);
    void kiir() const;
    void push_back(int);
    void pop_back();
    void trim();
};

#endif